package com.mudra.instantmudra.retrofit;

import com.google.gson.annotations.SerializedName;

public class WalletModel {

    @SerializedName("statusDesc")
    val statusDesc: String? = ""

    @SerializedName("createdDate")
    val createdDate: String? = ""

    @SerializedName("productCode")
    val productCode: String? = ""

    @SerializedName("txnType")
    val txnType: String? = ""

    @SerializedName("txnAmount")
    val txnAmount: String? = ""

    @SerializedName("customeridentIfication")
    val customeridentIfication: String?=""

    @SerializedName("status")
    val status: String? = ""

    @SerializedName("txnId")
    val txnId: String? = ""

    @SerializedName("username")
    val username: String? = ""

    @SerializedName("param_a")
    val param_a: String? = ""

    @SerializedName("param_b")
    val param_b: String? = ""

    @SerializedName("param_c")
    val param_c: String? = ""


    constructor(
        statusDesc: String,
        createdDate: String,
        productCode: String,
        txnType: String,
        txnAmount: String,
        customeridentIfication: String,
        status: String,
        txnId: String,
        username: String,
        param_a: String,
        param_b: String,
        param_c: String
    ) {
//        this.statusDesc = statusDesc
//        this.createdDate = createdDate
//        this.productCode = productCode
//        this.txnType = txnType
//        this.txnAmount = txnAmount
//        this.customeridentIfication = customeridentIfication
//        this.status = status
//        this.txnId = txnId
//        this.username = username
//        this.param_a = param_a
//        this.param_b = param_b
//        this.param_c = param_c
    }

    override fun toString(): String {
        return "WalletModel(statusDesc=$statusDesc," +
                " createdDate=$createdDate, " +
                "productCode=$productCode, " +
                "txnType=$txnType, " +
                "txnAmount=$txnAmount," +
                " customeridentIfication=$customeridentIfication, " +
                "status=$status, " +
                "txnId=$txnId, " +
                "username=$username, " +
                "param_a=$param_a, " +
                "param_b=$param_b," +
                " param_c=$param_c)"
    }


}
