package com.mudra.instantmudra.retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    companion object {
        val BASE_URL="https://txnprc.instantmudra.co.in/appApi/";
        var retrofit: Retrofit? = null
        fun getClient(): Retrofit? {
            if (retrofit == null) {

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
//                    .client(okhttp3.OkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            }

            return retrofit
        }
    }
}
