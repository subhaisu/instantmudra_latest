package com.mudra.instantmudra.retrofit;

import com.mudra.instantmudra.model.Cashout_response_model
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiInterface {

        @POST("getWalletBalance")
        fun getWallet(@Body mobile_json: HashMap<String,String>):Call<WalletModel>

        @POST("processPayout")
        fun cash_outapi(@Body request_json: HashMap<String,String>):Call<Cashout_response_model>


}
