package com.mudra.instantmudra;

import android.Manifest;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.matm.matmsdk.Utils.Session;
import com.mudra.instantmudra.bluetooth.SharePreferenceClass;
import com.mudra.instantmudra.setting.BluetoothConnectorActivity;


import java.util.HashMap;
import java.util.UUID;

import fr.ganfra.materialspinner.MaterialSpinner;


public class SettingsActivity extends AppCompatActivity {

    private MaterialSpinner deviceSpinner;
    private static final String[] ITEMS = {"EVOLUTE", "BLUPRINTS"};
    Session session;

    SessionManager sessionManager;
    TextView  device_name;
    boolean first_run = true;
    boolean  from_RD = false;
    SharePreferenceClass sharePreferenceClass;
    private static final UUID MY_UUID = UUID.fromString("0000110E-0000-1000-8000-00805F9B34FB");
    /**
     * The BluetoothAdapter is the gateway to all bluetooth functions
     **/
    protected BluetoothAdapter bluetoothAdapter = null;

    /**
     * We will write our message to the socket
     **/
    protected BluetoothSocket socket = null;

    /**
     * The Bluetooth is an external device, which will receive our message
     **/
    BluetoothDevice blueToothDevice = null;
    String _token, _admin;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setToolbar();
        session = new Session(this);

        sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> user = sessionManager.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = sessionManager.getUserSession();
        _userName = user_.get(sessionManager.userName);

        sharePreferenceClass = new SharePreferenceClass(SettingsActivity.this);

        deviceSpinner = findViewById(R.id.btdeviceSpinner);
        device_name = findViewById(R.id.bt_device_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ITEMS);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        deviceSpinner.setAdapter(adapter);


        if ( ConstantClass.selected_fingerPrint != null) {
            device_name.setVisibility(View.VISIBLE);
            device_name.setText("Connected Device : " + ConstantClass.selected_fingerPrint.getName());

        } else {
            device_name.setVisibility(View.INVISIBLE);
        }


        deviceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {



                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                            from_RD = true;
                            Intent in = new Intent(SettingsActivity.this, BluetoothConnectorActivity.class);
                            startActivity(in);
                            finish();
                        } else {
                            from_RD = true;
                            Intent in = new Intent(SettingsActivity.this, BluetoothConnectorActivity.class);
                            startActivity(in);
                            finish();
                        }
                    } else {

                    }
                }
                if(position == 1){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED}, 1001);
                            Toast.makeText(getApplicationContext(), "Please Grant all the permissions", Toast.LENGTH_LONG).show();
                            from_RD = true;
                            Intent in = new Intent(SettingsActivity.this, BluetoothConnectorActivity.class);
                            startActivity(in);
                            finish();
                        } else {
                            from_RD = true;
                            Intent in = new Intent(SettingsActivity.this, BluetoothConnectorActivity.class);
                            startActivity(in);
                            finish();
                        }
                    } else {

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    public void showAlert(Context context) {
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(context);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Please download the MATM service app from the playstore.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Download Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        redirectToPlayStore();
                    }
                })
                .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void redirectToPlayStore() {
        Uri uri = Uri.parse("market://details?id=com.matm.matmservice");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=com.matm.matmservice")));
        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();

        if (from_RD) {

            if ( Constants.selected_fingerPrint != null) {
                device_name.setVisibility(View.VISIBLE);
                device_name.setText("Connected Device : " + Constants.selected_fingerPrint.getName());

            } else {
                device_name.setVisibility(View.INVISIBLE);
            }
        }

    }
*/





    /*
   app installation check
   */
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }


    private void setToolbar() {
        Toolbar mToolbar = findViewById(R.id.toolbar);
        mToolbar.setTitle(getResources().getString(R.string.settings));
        mToolbar.inflateMenu(R.menu.bank_menu);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.action_close) {
                    finish();
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}