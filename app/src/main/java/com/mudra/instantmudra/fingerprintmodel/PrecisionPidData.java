package com.mudra.instantmudra.fingerprintmodel;






import com.mudra.instantmudra.fingerprintmodel.uid.Data;
import com.mudra.instantmudra.fingerprintmodel.uid.Skey;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "PrecisionPidData")
public class PrecisionPidData {

    public PrecisionPidData() {
    }
    @Element(name = "Resp",required = false)
                public Resp _Resp;

    @Element(name = "DeviceInfo",required = false)
    public PrecisionDeviceInfo _DeviceInfo;

    @Element(name = "Skey", required = false)
    public Skey _Skey;

     @Element(name = "Hmac",required = false)
    public String Hmac;

     @Element(name = "Data", required = false)
    public Data _Data;

}
