package com.mudra.instantmudra;

public interface ConnectionLostCallback {
    public void connectionLost();
}
