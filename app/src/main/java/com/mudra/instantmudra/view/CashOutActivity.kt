package com.mudra.instantmudra.view

import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.mudra.instantmudra.R
import com.mudra.instantmudra.model.Cashout_response_model
import com.mudra.instantmudra.retrofit.ApiInterface
import com.mudra.instantmudra.retrofit.RetrofitClient
import com.mudra.instantmudra.retrofit.WalletModel
import kotlinx.android.synthetic.main.activity_dashbord.*
import org.json.JSONException
import retrofit2.Call
import retrofit2.Response

class CashOutActivity :AppCompatActivity(),RadioGroup.OnCheckedChangeListener{

    lateinit var wal1_bal_txt:TextView
    lateinit var back:ImageView
    private var PRIVATE_MODE = 0
    private val WALLET_BAL_TWO = "wallet_two_balance"
    private val USERNAME = "UserName"
    private val PREF_NAME = "InstantMudra"
    private var dialog: ProgressDialog? = null
    lateinit var apiInterface:ApiInterface
    lateinit var mToolbar:Toolbar
    lateinit var transferAmount_btn:Button
    lateinit var addAmountEdit:EditText
    lateinit var rg_select_bene:RadioGroup
    lateinit var rb_select_neft:RadioButton
    lateinit var rb_select_imps:RadioButton

    var referenceID:Long=0
    var walletBal_two:String=""
    var userNameStr:String=""
    var trans_Mode:String="IMPS"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet_tobank)

        wal1_bal_txt=findViewById(R.id.wal1_bal_edt)
        back=findViewById(R.id.back)
        transferAmount_btn=findViewById(R.id.transferAmount)
        addAmountEdit=findViewById(R.id.addAmountEdit)
        rg_select_bene=findViewById(R.id.rg_select_bene)
        rb_select_neft=findViewById(R.id.rb_select_neft)
        rb_select_imps=findViewById(R.id.rb_select_imps)

        userNameStr = getValueString(USERNAME).toString()


        setUpToolBar("CashOut")

        callWallet()
        exeTime()


        rg_select_bene.setOnCheckedChangeListener(this)


        transferAmount_btn.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {

                if (addAmountEdit.text.toString().equals("") || addAmountEdit.text.toString().equals("0")){
                    Toast.makeText(this@CashOutActivity,"Please enter valid amount !",Toast.LENGTH_LONG).show()
                }
                else if (trans_Mode.equals("")){
                    Toast.makeText(this@CashOutActivity,"Please select transaction mode !",Toast.LENGTH_LONG).show()

                }
                else{
                    referenceID=System.currentTimeMillis()

                    call_cashout_api(""+referenceID,
                        userNameStr,
                        trans_Mode,
                        addAmountEdit.text.toString())

                }
            }
        })

    }

    private fun setUpToolBar(title: String) {
        // Inflate the layout for this fragment
        mToolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(mToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.subtitle = ""
        supportActionBar!!.title=title
        mToolbar.getNavigationIcon()?.setTint(getResources().getColor(R.color.white));

        mToolbar.setNavigationOnClickListener(View.OnClickListener {
            onBackPressed()
            //What to do on back clicked
        })
    }

    fun getValueString(KEY_NAME: String): String? {
        val sharedPref: SharedPreferences = getSharedPreferences(USERNAME, PRIVATE_MODE)
        return sharedPref.getString(KEY_NAME, null)
    }

    fun removeUseName(KEY_NAME: String, value: String) {
        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val editor: SharedPreferences.Editor = sharedPref.edit()
        //editor.putString(KEY_NAME, value)
        editor.remove(KEY_NAME)
        editor.clear()
        editor.commit()
    }

    fun exeTime() {
        var start = System.currentTimeMillis()
//        code.accept(null)
        referenceID = System.currentTimeMillis()
        println("Execution time: " + referenceID + "ms")
//        Toast.makeText(this,""+referenceID,Toast.LENGTH_LONG).show()
    }

    fun callWallet(){
        try {
            dialog = ProgressDialog(this@CashOutActivity)
            dialog!!.setMessage("please wait..")
            dialog!!.setCancelable(false)
            dialog!!.show()

//        var jobj = JSONObject()
            val mobile_hashmap = HashMap<String, String>()

            try {
                mobile_hashmap.put("mobile_no", userNameStr)
//            jobj.put("mobile_no", userNameStr)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

            apiInterface = RetrofitClient.getClient()!!.create(ApiInterface::class.java)
            var call = apiInterface.getWallet(mobile_hashmap)
            call.enqueue(object : retrofit2.Callback<WalletModel> {
                override fun onFailure(call: retrofit2.Call<WalletModel>, t: Throwable) {

                    Toast.makeText(this@CashOutActivity, "Failed !!", Toast.LENGTH_LONG).show();
                    dialog!!.dismiss()

                }

                override fun onResponse(
                    call: retrofit2.Call<WalletModel>,
                    response: retrofit2.Response<WalletModel>
                ) {

                    dialog!!.dismiss()
                    val wallet_res = response.body()!!
                    var wallet_one_bal = wallet_res.param_a
                    var wallet_two_bal = wallet_res.param_b
                    var success = wallet_res.status

                    Log.d("Response", wallet_res.toString())
//
//                Toast.makeText(this@DashbordActivity, success, Toast.LENGTH_LONG).show();


                    if (wallet_two_bal == null || wallet_one_bal.equals("null", false)) {

                        wal1_bal_txt.setText("Balance : \u20B9 "+0.0)


                    } else {
                        wal1_bal_txt.setText("Balance : \u20B9 "+wallet_two_bal)


                    }

                }

            })
        }catch (e:Exception){
            dialog!!.dismiss()

        }

    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

        val checkedRadioButton = group?.findViewById(group.checkedRadioButtonId) as? RadioButton
        checkedRadioButton?.let {

            if (checkedRadioButton.isChecked)
              trans_Mode=""+checkedRadioButton?.text
//                Toast.makeText(applicationContext, "RadioButton: ${checkedRadioButton?.text}", Toast.LENGTH_LONG).show()
        }
    }
    fun call_cashout_api(refNo:String,mobileNo:String,transactionType:String,amount_transfer:String) {
        try {
            dialog = ProgressDialog(this@CashOutActivity)
            dialog!!.setMessage("please wait..")
            dialog!!.setCancelable(false)
            dialog!!.show()

            val cashout_request_hashmap = HashMap<String, String>()

            try{

//            cashout_request_hashmap.put("Txn_Ref_no",""+refNo)
            cashout_request_hashmap.put("mobile_no",""+mobileNo)
            cashout_request_hashmap.put("txn_type",""+transactionType)
            cashout_request_hashmap.put("amount",""+amount_transfer)

            } catch (e: JSONException) {
                dialog!!.dismiss()
                e.printStackTrace()
            }
            apiInterface = RetrofitClient.getClient()!!.create(ApiInterface::class.java)
            var call = apiInterface.cash_outapi(cashout_request_hashmap)

            call.enqueue(object :retrofit2.Callback<Cashout_response_model>{
                override fun onFailure(call: Call<Cashout_response_model>, t: Throwable) {

                    Toast.makeText(this@CashOutActivity, "Failed !!", Toast.LENGTH_LONG).show();
                    dialog!!.dismiss()

                }

                override fun onResponse(
                    call: Call<Cashout_response_model>,
                    response: Response<Cashout_response_model>
                ) {
                    dialog!!.dismiss()
                    val cashout_response = response.body()!!
                    var wallet_one_bal = cashout_response.cashoutDataobj?.prepaid_wallet_balance
                    var wallet_two_bal = cashout_response.cashoutDataobj?.postpaid_wallet_balance
                    var success = cashout_response.status
                    var successDesc = cashout_response.statusDesc
                    var refNo = cashout_response.cashoutDataobj?.txn_no
                    var amount = cashout_response.cashoutDataobj?.amount

                    if(amount== null){
                        amount = addAmountEdit.text.toString()
                    }


                    if (wallet_two_bal == null || wallet_one_bal.equals("null", false)) {

                        callWallet()

                    } else {
                        wal1_bal_txt.setText("Balance : \u20B9 "+wallet_two_bal)

                    }
                    addAmountEdit.setText("")
//                    Toast.makeText(this@CashOutActivity,successDesc,Toast.LENGTH_LONG).show()
                    val intent = Intent(this@CashOutActivity, Transaction_success_activity::class.java)
                    intent.putExtra("transaction_id", refNo)
                    intent.putExtra("transaction_desc", successDesc)
                    intent.putExtra("transaction_amount", amount)
                    startActivity(intent)


                }
            })

        }catch (e:Exception){
            dialog!!.dismiss()
        }
    }


}