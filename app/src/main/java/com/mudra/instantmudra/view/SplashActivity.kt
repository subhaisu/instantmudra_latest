package com.mudra.instantmudra.view

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.mudra.instantmudra.MainActivity
import com.mudra.instantmudra.R

class SplashActivity : AppCompatActivity() {

    val SPLASH_TIMEOUT = 3000L
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "InstantMudra"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        if (sharedPref.getBoolean(PREF_NAME, false)) {


            Handler().postDelayed({
                val i = Intent(this@SplashActivity, DashbordActivity::class.java)
                startActivity(i)
                finish()

            }, SPLASH_TIMEOUT)


        } else {
            Handler().postDelayed({
                val i = Intent(this@SplashActivity, MainActivity::class.java)
                startActivity(i)
                finish()

            }, SPLASH_TIMEOUT)

        }


    }
}
