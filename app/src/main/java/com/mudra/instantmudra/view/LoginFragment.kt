package com.mudra.instantmudra.view

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.adapters.TextViewBindingAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.TaskExecutors
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import com.google.firebase.firestore.FirebaseFirestore
import com.mudra.instantmudra.R
import com.mudra.instantmudra.databinding.FragmentLoginBinding
import com.mudra.instantmudra.listener.BtnClickListener
import com.mudra.instantmudra.viewmodel.LoginViewModel
import android.content.SharedPreferences
import android.graphics.Color
import android.view.inputmethod.InputMethodManager
import java.util.*

import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class LoginFragment:Fragment(), View.OnClickListener {

    lateinit var loginView:View
    lateinit var loginViewmodel:LoginViewModel
    lateinit var myListener: BtnClickListener
    //var mAuth:FirebaseAuth? = null
    var mAuth = FirebaseAuth.getInstance()
    var otp:String = ""
    var PhoneStr:String = ""
    var mVerificationId:String =""
    lateinit var loginBinder :FragmentLoginBinding
    lateinit var pd:ProgressDialog
    private var PRIVATE_MODE = 0
    private val PREF_NAME = "InstantMudra"
    private val USERNAME = "UserName"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewmodel= ViewModelProviders.of(activity!!).get(LoginViewModel::class.java)
        //mAuth = FirebaseAuth.getInstance()
        mAuth.setLanguageCode(Locale.getDefault().language)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        loginBinder   = DataBindingUtil.inflate(inflater, R.layout.fragment_login,container,false)

        loginView = loginBinder.root
        myListener = BtnClickListener()

        loginBinder.handlers  = myListener


        loginBinder.SubmitBtn.setOnClickListener(View.OnClickListener {
            val phoneNo = loginBinder.mobileNo.text.toString()
            val password  = loginBinder.password.text.toString()
            otp = loginBinder.otpField.text.toString()
            PhoneStr = phoneNo
            it.hideKeyboard();


            loginBinder.mobileNo.addTextChangedListener(object:
                TextWatcher {override fun afterTextChanged(s: Editable?) {
                var str:String = s.toString()
                if (s != null) {
                    if(s.length==10){
                        loginBinder.SubmitBtn.isEnabled = true
                        loginBinder.verifyBtn.isEnabled = false
                        loginBinder.password.setText("")
                    }
                }
            }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

            })

            loginBinder.otpField.addTextChangedListener(object:
                TextWatcher {override fun afterTextChanged(s: Editable?) {

                var str:String = s.toString()
                if (s != null) {
                    if(s.length>0){
                        loginBinder.verifyBtn.isEnabled = true
                        loginBinder.verifyBtn.setBackgroundColor(Color.parseColor("#D1171D"))
                    }
                }

            }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }
            })


            
            //Its For tempory use
            if(phoneNo.length!=10){
                Snackbar.make(loginView,"Enter valid phoneNo.",Snackbar.LENGTH_SHORT).show()
            }
            /*else if(password.length<6){
                Snackbar.make(loginView,"Enter Proper password",Snackbar.LENGTH_SHORT).show()
            }*/

            else{
                var  task:MyAsyncTask = MyAsyncTask()
                task.execute("www.nplix.com","www.debugandroid.com")
               // sendVerificationCode(phoneNo)
            }
        })
        loginBinder.verifyBtn.setOnClickListener(View.OnClickListener {


            if(loginBinder.otpField.text.toString().equals(otp,ignoreCase = true)){
                verifyVerificationCode(otp)
            }else{
                Toast.makeText(activity,"Incorrect OTP .",Toast.LENGTH_SHORT).show();
            }

        })
        return loginView
    }

    private fun sendVerificationCode(phoneNo: String) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber("+91"+phoneNo,
            60,
            TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD,
            mCallbacks
        )

    }


    //the callback to detect the verification status
    private val mCallbacks: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                //Getting the code sent by SMS
                val code = phoneAuthCredential.smsCode
                //sometime the code is not detected automatically
                //in this case the code will be null
                //so user has to manually enter the code
                if (code != null) {
                    otp = code
                    loginBinder.otpField.setText(otp)
                    loginBinder.verifyBtn.isEnabled = true
                    loginBinder.SubmitBtn.isEnabled = false
                    //verifying the code
                   // verifyVerificationCode(code)
                }
                else{
                    println("Not getting code .")

                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                var msg: String? = e.message
                if (msg != null) {
                    Snackbar.make(loginView, msg,Snackbar.LENGTH_SHORT).show()
                }
            }

            override fun onCodeSent(s: String, forceResendingToken: PhoneAuthProvider.ForceResendingToken) {
                super.onCodeSent(s, forceResendingToken)

                //storing the verification id that is sent to the user
                mVerificationId = s
                Snackbar.make(loginView,"Code has been sent Successfully.",Snackbar.LENGTH_SHORT).show()
            }

        }


    private fun verifyVerificationCode(code: String) {
        //creating the credential
        val credential = PhoneAuthProvider.getCredential(mVerificationId, code)

        //signing the user
        signInWithPhoneAuthCredential(credential)
    }
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {

        mAuth.signInWithCredential(credential).addOnCompleteListener(activity!!,
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {
                        //verification successful we will start the profile activity
                        val sharedPref: SharedPreferences = activity!!.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
                        val editor = sharedPref.edit()
                        editor.putBoolean(PREF_NAME, true)
                        editor.apply()
                        saveUseName(USERNAME,PhoneStr)


                        val intent = Intent(activity, DashbordActivity::class.java)
                        //intent.putExtra("userName",PhoneStr)
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                        startActivity(intent)
                    } else {

                        Snackbar.make(loginView,"Failure..",Snackbar.LENGTH_SHORT).show()
                    }
                })
    }

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }


    inner class MyAsyncTask : AsyncTask<String, Int, Int>(){

        //Override the doInBackground method
        override fun doInBackground(vararg params: String?): Int {
            val count:Int=params.size


            val db: FirebaseFirestore = FirebaseFirestore.getInstance()

            val docRef = db.collection("User").document("LA")
            docRef.get()
                .addOnSuccessListener { document ->
                    if (document != null) {
                        pd.dismiss()
                       var hasdata:HashMap<String,String> = HashMap()
                        hasdata = document.data as HashMap<String, String>

                        if (hasdata.containsKey("+91"+PhoneStr)) {

                            sendVerificationCode(PhoneStr)

                            println("YES.. GOT IT")
                        } else {

                           Toast.makeText(activity,"Phone no is not Registered. ",Toast.LENGTH_SHORT).show()
                            pd.dismiss()
                        }


                    } else {
                        Log.d("Fail", "No such document")
                        pd.dismiss()
                    }
                }
                .addOnFailureListener { exception ->
                    Log.d("Fail", "get failed with ", exception)
                    pd.dismiss()
                }
            return count;

        }
        override fun onPreExecute() {
            super.onPreExecute()
            pd = ProgressDialog(activity)
            pd.setMessage("Loading...")
            pd.setCancelable(false)
            pd.show()


        }
        override fun onPostExecute(result: Int?) {
            super.onPostExecute(result)


        }

    }

    fun saveUseName(KEY_NAME: String, value: String) {
        val sharedPref: SharedPreferences = activity!!.getSharedPreferences(USERNAME, PRIVATE_MODE)

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, value)

        editor.commit()
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

}