package com.mudra.instantmudra.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.mudra.instantmudra.R

public class Transaction_success_activity :AppCompatActivity(){

    var transaction_id:String=""
    var transaction_desc:String=""
    var transaction_amount:String=""

    lateinit var success_img:ImageView
    lateinit var success_txt:TextView
    lateinit var transactionID:TextView
    lateinit var amount_txt:TextView
    lateinit var main_ll:LinearLayout
    lateinit var trasaction_id_ll:LinearLayout
    lateinit var submit_btn:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.transaction_success_layout)

        transactionID=findViewById(R.id.transaction_id)
        success_txt=findViewById(R.id.success_txt)
        success_img=findViewById(R.id.success_img)
        amount_txt=findViewById(R.id.amount)
        main_ll=findViewById(R.id.main_ll)
        trasaction_id_ll=findViewById(R.id.trasaction_id_ll)
        submit_btn=findViewById(R.id.submit_btn)

//        val intent=getIntent()
        val bundle: Bundle? = intent.extras

        transaction_id= bundle?.get("transaction_id").toString()
        transaction_desc= bundle?.get("transaction_desc").toString()
        transaction_amount= bundle?.get("transaction_amount").toString()

        if (transaction_id.equals("null") ||transaction_id.equals("") || transaction_id==null){
//            main_ll.setBackgroundColor(R.color.instamudra_red)
            main_ll.setBackgroundColor(getResources().getColor(R.color.instamudra_red));
            success_img.setImageResource(R.drawable.failed_cross)
            if (!transaction_desc.equals("")){
                success_txt.setText(transaction_desc)
                amount_txt.setText("₹ "+transaction_amount)

            }
            else{
                success_txt.setText("Transaction Failed")
                amount_txt.setText("₹ "+transaction_amount)

            }

            trasaction_id_ll.visibility=View.GONE


        }
        else{

            main_ll.setBackgroundColor(getResources().getColor(R.color.red));

            success_img.setImageResource(R.drawable.success_tick)
            transactionID.setText(transaction_id)
            amount_txt.setText("₹ "+transaction_amount)
            success_txt.setText(transaction_desc)

        }

        submit_btn.setOnClickListener(object :View.OnClickListener{
            override fun onClick(v: View?) {

                var intent= Intent(this@Transaction_success_activity,DashbordActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
        })


    }
}