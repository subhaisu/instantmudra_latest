package com.mudra.instantmudra.view

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import com.matm.matmsdk.Bluetooth.BluetoothActivity
import com.matm.matmsdk.MPOS.BluetoothServiceActivity
import com.matm.matmsdk.MPOS.PosActivity
import com.matm.matmsdk.MPOS.PosServiceActivity
import com.matm.matmsdk.Utils.SdkConstants
import com.matm.matmsdk.aepsmodule.AEPSHomeActivity
import com.matm.matmsdk.aepsmodule.BioAuthActivity
import com.mudra.instantmudra.ConstantClass
import com.mudra.instantmudra.ConstantClass.bluetoothConnector
import com.mudra.instantmudra.R
import com.mudra.instantmudra.SettingsActivity
import com.mudra.instantmudra.reports.ReportDashboardActivity
import com.mudra.instantmudra.retrofit.ApiInterface
import com.mudra.instantmudra.retrofit.RetrofitClient
import com.mudra.instantmudra.retrofit.WalletModel
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class DashbordActivity : AppCompatActivity(),NavigationView.OnNavigationItemSelectedListener {

    lateinit var amnt_txt: EditText
    lateinit var aeps_submit_btn: Button
    lateinit var matm_submit_btn: Button
    lateinit var aepsOne_submit_btn: Button
    lateinit var btnPair: Button
    lateinit var logout: ImageView
    lateinit var rg_trans_type: RadioGroup
    lateinit var rb_cw: RadioButton
    lateinit var rb_be: RadioButton
    lateinit var wallet_one_txt: TextView
    lateinit var das_user_balance: TextView
    lateinit var wallet_two_txt: TextView
    lateinit var das_user_balance2: TextView
    lateinit var reload_wallet1: ImageView
    lateinit var reload_wallet2: ImageView
    lateinit var menu_nav: ImageView
    lateinit var navigationView: NavigationView
    lateinit var drawer: DrawerLayout
    var strTransType:String = ""
    var wallet_bal_two:String = ""
    var respflag : Boolean = false
    var userNameStr: String =""
    var manufactureFlag: String =""

    private var PRIVATE_MODE = 0
    private val USERNAME = "UserName"
    private val WALLET_BAL_TWO = "wallet_two_balance"
    private val GPS_REQUEST_CODE = 101
    private val PREF_NAME = "InstantMudra"
    var musbManager: UsbManager? = null
    private var usbDevice: UsbDevice? = null
    private var dialog: ProgressDialog? = null

    //ApiInterface apinterface;
    lateinit var apiInterface:ApiInterface

    var encryptedData : String = "TYqmJRyB%2B4Mb39MQf%2BPqVpG%2BMXYkFjv7FvFq5zSop426IBfOKVTFtcsgZDUCORAu%2FDJvr85SGAUeQVWgRINTI5teZqYzUL1nyFMcf1eO69A%3D"
    private val client = OkHttpClient()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.drawer_layout)
        musbManager = getSystemService(Context.USB_SERVICE) as UsbManager
        amnt_txt = findViewById(R.id.amnt_txt)
        aeps_submit_btn = findViewById(R.id.aeps_submit_btn)
        matm_submit_btn = findViewById(R.id.matm_submit_btn)
        rb_cw = findViewById(R.id.rb_cw)
        rb_be = findViewById(R.id.rb_be)
        wallet_one_txt = findViewById(R.id.wallet_one_txt)
        wallet_two_txt = findViewById(R.id.wallet_two_txt)
        das_user_balance = findViewById(R.id.das_user_balance)
        das_user_balance2 = findViewById(R.id.das_user_balance2)
        reload_wallet1 = findViewById(R.id.reload_wallet1)
        reload_wallet2 = findViewById(R.id.reload_wallet2)
        navigationView = findViewById(R.id.nav_view)
        drawer = findViewById(R.id.drawer_layout)

        menu_nav = findViewById(R.id.menu_nav)
        rg_trans_type = findViewById(R.id.rg_trans_type)
        btnPair = findViewById(R.id.btnPair)
        logout = findViewById(R.id.logout)
        aepsOne_submit_btn = findViewById(R.id.aepsOne_submit_btn)
        val headerview = navigationView.getHeaderView(0)

//        apiInterface= RetrofitClient.getClient().create(ApiInterface::class.java)
//        apiInterface = RetrofitClient.getClient()!!.create(ApiInterface::class.java)






        val intent = getIntent()
        //userNameStr = "8917585029"//test
        userNameStr = getValueString(USERNAME).toString()

        ConstantClass.ENCRIPTED_DATA = encryptedData
        ConstantClass.LOGIN_ID = userNameStr

        setupPermissions()

        navigationView.setNavigationItemSelectedListener(this)
        
        menu_nav.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {

                drawer.openDrawer(GravityCompat.START)
            }
        })


        
        Log.d("Error",userNameStr)
        rg_trans_type.setOnCheckedChangeListener {
                group, checkedId ->
            when (checkedId) {
                R.id.rb_cw -> {
                    strTransType = "Cash Withdrawal"
                    amnt_txt.setText("")
                    amnt_txt.isEnabled = true
                    amnt_txt.visibility = View.VISIBLE
                    amnt_txt.inputType = InputType.TYPE_CLASS_NUMBER
                }
                R.id.rb_be -> {
                    strTransType = "Balance Enquiry"
                    amnt_txt.setText("")
                    amnt_txt.visibility = View.GONE
                    amnt_txt.isClickable = false
                    amnt_txt.isEnabled = false
                }
            }
        }
        matm_submit_btn.setOnClickListener {
            if (rb_cw.isChecked) {
                SdkConstants.transactionType = SdkConstants.cashWithdrawal
                SdkConstants.transactionAmount = amnt_txt.text.toString()
                if (amnt_txt.text.toString().trim().length == 0) {
                    Toast.makeText(this, "Please enter minimum Rs 100 ", Toast.LENGTH_SHORT).show()
                } else if (amnt_txt.text.toString().toInt() < 100) {
                    Toast.makeText(this, "Please enter minimum Rs 100 ", Toast.LENGTH_SHORT).show()
                } else {
                    if(SdkConstants.BlueToothPairFlag.equals("1",ignoreCase = true)) {
                        //if (PosActivity.isBlueToothConnected(this)) {
                            SdkConstants.paramA = "Instantmudra"
                            SdkConstants.paramB = "Branch1"
                            SdkConstants.paramC = ""
                            SdkConstants.loginID = userNameStr
                            SdkConstants.encryptedData = encryptedData
                            SdkConstants.applicationType =""

                            val intent = Intent(this, PosServiceActivity::class.java)
                            startActivityForResult(intent, SdkConstants.MATM_REQUEST_CODE)
                            respflag = true
                        /*} else {
                            Toast.makeText(this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show()
                        }*/
                    }else{
                        Toast.makeText(this, "Please pair the bluetooth device!", Toast.LENGTH_SHORT).show()

                    }
                }
            }
            if (rb_be.isChecked) {
                if(SdkConstants.BlueToothPairFlag.equals("1",ignoreCase = true)) {
               // if (PosActivity.isBlueToothConnected(this)) {
                    SdkConstants.transactionType = SdkConstants.balanceEnquiry
                    SdkConstants.transactionAmount = "0"
                    SdkConstants.paramA = "InstantMudra"
                    SdkConstants.paramB = "Branch1"
                    SdkConstants.paramC = ""
                    SdkConstants.loginID = userNameStr
                    SdkConstants.encryptedData = encryptedData
                    SdkConstants.applicationType =""

                    val intent = Intent(this, PosServiceActivity::class.java)
                    startActivityForResult(intent, SdkConstants.MATM_REQUEST_CODE)
                    respflag = true
               /* }
                else {
                    Toast.makeText(this, "Please pair the bluetooth device", Toast.LENGTH_SHORT).show()
                }*/
                }else{
                    Toast.makeText(this, "Please pair the bluetooth device!", Toast.LENGTH_SHORT).show()

                }
            }
        }

        //-----------Wallet API

//        CallWalletAPI("https://txnprc.instantmudra.co.in/appApi/getWalletBalance")

        callWallet()

        //---------AEPS1------------

        aepsOne_submit_btn.setOnClickListener {
            if(bluetoothConnector){
                callAEPS1SDKApp()
            }else {
                if (biometricDeviceConnect()) {
                    callAEPS1SDKApp()
                } else {
                    Toast.makeText(
                        this@DashbordActivity,
                        "Connect your device.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }




        aeps_submit_btn.setOnClickListener {

            if(bluetoothConnector){
                callAEPS2SDKAPP()
            }else{
                if (biometricDeviceConnect()) {
                    callAEPS2SDKAPP()
                } else {
                    Toast.makeText(this@DashbordActivity, "Connect your device.", Toast.LENGTH_SHORT).show()
                }

            }
            /*if (biometricDeviceConnect()) {
                callAEPS2SDKAPP()
            } else {
                Toast.makeText(this@DashbordActivity, "Connect your device.", Toast.LENGTH_SHORT).show()
            }*/
        }


        btnPair.setOnClickListener {
            val intent = Intent(this, BluetoothServiceActivity::class.java)
            //intent.putExtra("user_id","33221");
            SdkConstants.loginID = userNameStr
            SdkConstants.encryptedData = encryptedData
            SdkConstants.applicationType =""

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                    requestPermissions(arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.BLUETOOTH_PRIVILEGED
                    ), 1001
                    )
                    Toast.makeText(applicationContext, "Please Grant all the permissions", Toast.LENGTH_LONG).show()
                } else {
                    startActivity(intent)
                }
            } else {
                startActivity(intent)
            }
        }
        logout.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //Showing Alert Dialog
                ShowAlert()


             }
        })

        reload_wallet1.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //Refreshing wallet
                callWallet()

            }
        })
        reload_wallet2.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                //Refreshing wallet
                callWallet()

            }
        })



    }

    private fun CallWalletAPI(url:String) {

        val jobj = JSONObject()
        try {
            jobj.put("mobile_no", userNameStr)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        val body = jobj.toString().toRequestBody("application/json; charset=utf-8".toMediaType())
//        val body = jobj.toString().toRequestBody("application/json; charset=utf-8".toMediaTypeOrNull())

        val request = Request.Builder()
            .url(url)
            .post(body)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d("error",e.toString())
            }

            override fun onResponse(call: Call, response: Response) {
                Log.d("Success",response.body.toString())
                Log.d("Call",call.toString())
                if (response.isSuccessful){
//                    try {
//                        val jsonObject = JSONObject(response.body.toString())
//
//                        Toast.makeText(this@DashbordActivity,status,Toast.LENGTH_LONG).show();
//
//                    }catch (e:Exception){
//
//                    }

                }
            }
        })



    }

    fun ShowAlert(){
        try {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Logout")
            builder.setMessage("Do you want to logout Application.")
            builder.setPositiveButton("Yes") { dialog, which ->
                //Toast.makeText(applicationContext, "continuar", Toast.LENGTH_SHORT).show()
                SdkConstants.BlueToothPairFlag = "0";
                removeUseName(PREF_NAME, "");
                finish();
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
//            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.instamudra_red))

        }catch(e:Exception){

        }

    }

    private fun callAEPS2SDKAPP(){
        SdkConstants.transactionAmount =
            amnt_txt.text.toString().trim ()
        if (rb_cw.isChecked) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal
            SdkConstants.transactionAmount = amnt_txt.text.toString()
            if (amnt_txt.text.toString().trim ().length == 0) {
                Toast.makeText(
                    this,
                    "Please enter minimum Rs 1 ",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (amnt_txt.text.toString().toInt() < 1) {
                Toast.makeText(
                    this,
                    "Please enter minimum Rs 1 ",
                    Toast.LENGTH_SHORT
                ).show()
            }  else {
                SdkConstants.paramA = "InstantMudra"
                SdkConstants.paramB = "Branch1"
                SdkConstants.paramC = ""
                SdkConstants.loginID = userNameStr
                SdkConstants.MANUFACTURE_FLAG = manufactureFlag
                SdkConstants.DRIVER_ACTIVITY = "com.mudra.instantmudra.DriverActivity"
                SdkConstants.encryptedData = encryptedData
                val intent = Intent(this, BioAuthActivity::class.java)
                //intent.putExtra("manufaturName","")
                startActivityForResult(intent, SdkConstants.REQUEST_CODE)
            }
        }
        if (rb_be.isChecked) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry
            SdkConstants.transactionAmount = "1"
            SdkConstants.paramA = "InstantMudra"
            SdkConstants.paramB = "Branch1"
            SdkConstants.paramC = ""
            SdkConstants.loginID = userNameStr
            SdkConstants.encryptedData = encryptedData
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag
            SdkConstants.DRIVER_ACTIVITY = "com.mudra.instantmudra.DriverActivity"
            val intent = Intent(this, BioAuthActivity::class.java)
            // intent.putExtra("manufaturName","")
            startActivityForResult(intent, SdkConstants.REQUEST_CODE)
        }
    }

    private fun callAEPS1SDKApp(){
        SdkConstants.transactionAmount =
            amnt_txt.text.toString().trim ()
        if (rb_cw.isChecked) {
            SdkConstants.transactionType = SdkConstants.cashWithdrawal
            SdkConstants.transactionAmount = amnt_txt.text.toString()
            if (amnt_txt.text.toString().trim ().length == 0) {
                Toast.makeText(
                    this,
                    "Please enter minimum Rs 1 ",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (amnt_txt.text.toString().toInt() < 1) {
                Toast.makeText(
                    this,
                    "Please enter minimum Rs 1 ",
                    Toast.LENGTH_SHORT
                ).show()
            }  else {
                SdkConstants.paramA = "InstantMudra"
                SdkConstants.paramB = "Branch1"
                SdkConstants.paramC = ""
                SdkConstants.loginID = userNameStr
                SdkConstants.MANUFACTURE_FLAG = manufactureFlag
                SdkConstants.DRIVER_ACTIVITY = "com.mudra.instantmudra.DriverActivity"
                SdkConstants.encryptedData = encryptedData
                val intent = Intent(this, AEPSHomeActivity::class.java)
                // intent.putExtra("manufaturName","")
                startActivityForResult(intent, SdkConstants.REQUEST_CODE)
            }
        }
        if (rb_be.isChecked) {
            SdkConstants.transactionType = SdkConstants.balanceEnquiry
            SdkConstants.transactionAmount = "1"
            SdkConstants.paramA = "InstantMudra"
            SdkConstants.paramB = "Branch1"
            SdkConstants.paramC = ""
            SdkConstants.loginID = userNameStr
            SdkConstants.encryptedData = encryptedData
            SdkConstants.MANUFACTURE_FLAG = manufactureFlag
            SdkConstants.DRIVER_ACTIVITY = "com.mudra.instantmudra.DriverActivity"
            val intent = Intent(this, AEPSHomeActivity::class.java)
//                intent.putExtra("manufaturName","")
            startActivityForResult(intent, SdkConstants.REQUEST_CODE)
        }
    }


    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            GPS_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            GPS_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Log.i("Fail", "Permission has been denied by user")
                } else {
                    Log.i("Success", "Permission has been granted by user")
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && requestCode == Activity.RESULT_OK) {
            if (requestCode == SdkConstants.REQUEST_CODE) {
                val response = data!!.getStringExtra(SdkConstants.responseData)
                println("Response:$response")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (respflag) {
            val str = SdkConstants.responseData
           // Toast.makeText(applicationContext, str, Toast.LENGTH_LONG).show()
            respflag = false
        } else {
            println("No Data")
        }
    }

    fun getValueString(KEY_NAME: String): String? {
        val sharedPref: SharedPreferences = getSharedPreferences(USERNAME, PRIVATE_MODE)
        return sharedPref.getString(KEY_NAME, null)
    }
    fun saveUseName(KEY_NAME: String, value: String) {
        val sharedPref: SharedPreferences = this!!.getSharedPreferences(USERNAME, PRIVATE_MODE)

        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, value)

        editor.commit()
    }

    fun removeUseName(KEY_NAME: String, value: String) {
        val sharedPref: SharedPreferences = getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        val editor: SharedPreferences.Editor = sharedPref.edit()
        //editor.putString(KEY_NAME, value)
        editor.remove(KEY_NAME)
        editor.clear()
        editor.commit()
    }

    private fun biometricDeviceConnect(): Boolean {
        val connectedDevices =
            musbManager!!.deviceList
        usbDevice = null
        if (connectedDevices.isEmpty()) {
            deviceConnectMessgae()
            return false
        } else {
            for (device in connectedDevices.values) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (device != null && device.manufacturerName != null) {
                        usbDevice = device
                        manufactureFlag = usbDevice!!.getManufacturerName()!!
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun deviceConnectMessgae() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this)
        val dialog: AlertDialog =builder.setTitle("No device found")
//        val builder: AlertDialog.Builder
//        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            AlertDialog.Builder(
//                this@DashbordActivity,
//                android.R.style.Theme_Material_Light_Dialog_Alert
//            )
//        } else {
//            AlertDialog.Builder(this@DashbordActivity)
//        }
            .setCancelable(false)
            .setMessage("Unable to find biometric device connected . Please connect your biometric device for AEPS transactions.")
            .setPositiveButton(
                resources.getString(isumatm.androidsdk.equitas.R.string.ok_error)
            ) { dialog, which ->
                dialog.dismiss()
                // finish();
            }
            .show()
//        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.instamudra_red))

    }

     fun callWallet(){
         try {
             dialog = ProgressDialog(this@DashbordActivity)
             dialog!!.setMessage("please wait..")
             dialog!!.setCancelable(false)
             dialog!!.show()

//        var jobj = JSONObject()
             val mobile_hashmap = HashMap<String, String>()

             try {
                 mobile_hashmap.put("mobile_no", userNameStr)
//            jobj.put("mobile_no", userNameStr)
             } catch (e: JSONException) {
                 e.printStackTrace()
             }

             apiInterface = RetrofitClient.getClient()!!.create(ApiInterface::class.java)
             var call = apiInterface.getWallet(mobile_hashmap)
             call.enqueue(object : retrofit2.Callback<WalletModel> {
                 override fun onFailure(call: retrofit2.Call<WalletModel>, t: Throwable) {

                     Toast.makeText(this@DashbordActivity, "Failed !!", Toast.LENGTH_LONG).show();
                     dialog!!.dismiss()

                 }

                 override fun onResponse(
                     call: retrofit2.Call<WalletModel>,
                     response: retrofit2.Response<WalletModel>
                 ) {

                     dialog!!.dismiss()
                     val wallet_res = response.body()!!
                     var wallet_one_bal = wallet_res.param_a
                     var wallet_two_bal = wallet_res.param_b
                     var success = wallet_res.status

                     Log.d("Response", wallet_res.toString())
//
//                Toast.makeText(this@DashbordActivity, success, Toast.LENGTH_LONG).show();


                     if (wallet_one_bal == null || wallet_one_bal.equals("null", false)) {
//                    Toast.makeText(this@DashbordActivity, "NULL", Toast.LENGTH_LONG).show();
                         wallet_one_txt.setText("Prepaid")

                     } else {
                         wallet_one_txt.setText("Prepaid")
                         das_user_balance.setText("\u20A8. " + wallet_one_bal)
                     }

                     if (wallet_two_bal == null || wallet_one_bal.equals("null", false)) {
//                    Toast.makeText(this@DashbordActivity, "NULL", Toast.LENGTH_LONG).show();
                         wallet_two_txt.setText("PostPaid")


                     } else {
                         wallet_two_txt.setText("PostPaid")
                         das_user_balance2.setText("\u20A8. " + wallet_two_bal)

                         wallet_bal_two=wallet_two_bal

                     }

                 }

             })
         }catch (e:Exception){
             dialog!!.dismiss()

         }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when(item.itemId){

            R.id.cashout->{

                drawer.closeDrawer(GravityCompat.START)

                saveUseName(USERNAME,userNameStr)

                val intent=Intent(this,CashOutActivity::class.java)
                startActivity(intent)

            }

            R.id.report->{

//                Toast.makeText(
//                    this,
//                    "Report",
//                    Toast.LENGTH_SHORT
//                ).show()
                val intent = Intent(this, ReportDashboardActivity::class.java)
                intent.putExtra("mobileNo", userNameStr)
                startActivity(intent)


                drawer.closeDrawer(GravityCompat.START)

            }


            R.id.logout->{

                ShowAlert()  //For logout

                drawer.closeDrawer(GravityCompat.START)

            }

            R.id.setting->{

                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)

                drawer.closeDrawer(GravityCompat.START)

            }


        }

        return true
    }


}
