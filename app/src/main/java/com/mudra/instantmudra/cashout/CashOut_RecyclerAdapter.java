package com.mudra.instantmudra.cashout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mudra.instantmudra.R;

import java.util.ArrayList;

public class CashOut_RecyclerAdapter extends RecyclerView.Adapter<CashOut_RecyclerAdapter.ViewHolder> {

    ArrayList<CashoutItemMOdel> cashout_item_arr;
    Context context;

    public CashOut_RecyclerAdapter(ArrayList<CashoutItemMOdel> cashout_item_arr, Context context) {
        this.cashout_item_arr = cashout_item_arr;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cashout_report_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txn_no.setText(cashout_item_arr.get(position).getTxn_no());
        holder.txn_date.setText(cashout_item_arr.get(position).getTxn_date());
        holder.txn_amount.setText("\u20B9 "+cashout_item_arr.get(position).getAmount());
        holder.amount_after_txn.setText("\u20B9 "+cashout_item_arr.get(position).getBalance_after_txn());
        holder.txn_type.setText(cashout_item_arr.get(position).getTxn_type());
        holder.aadhar_no.setText(cashout_item_arr.get(position).getAeps_no());
        if (cashout_item_arr.get(position).getTxn_status().equalsIgnoreCase("success")){

            holder.txn_status.setText(cashout_item_arr.get(position).getTxn_status());
            holder.txn_status.setTextColor(context.getResources().getColor(R.color.color_report_green));
        }
        else if (cashout_item_arr.get(position).getTxn_status().equalsIgnoreCase("refunded")){
            holder.txn_status.setText(cashout_item_arr.get(position).getTxn_status());
            holder.txn_status.setTextColor(context.getResources().getColor(R.color.color_report_green));
        }
        else {
            holder.txn_status.setText(cashout_item_arr.get(position).getTxn_status());
            holder.txn_status.setTextColor(context.getResources().getColor(R.color.instamudra_red));
        }

    }

    @Override
    public int getItemCount() {
        return cashout_item_arr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txn_no,txn_date,txn_amount,amount_after_txn,txn_type,txn_status,aadhar_no;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txn_no=itemView.findViewById(R.id.txn_no);
            txn_date=itemView.findViewById(R.id.txn_date);
            txn_amount=itemView.findViewById(R.id.txn_amount);
            amount_after_txn=itemView.findViewById(R.id.amount_after_txn);
            txn_type=itemView.findViewById(R.id.txn_type);
            txn_status=itemView.findViewById(R.id.txn_status);
            aadhar_no = itemView.findViewById(R.id.aadhar_no);
        }
    }
}
