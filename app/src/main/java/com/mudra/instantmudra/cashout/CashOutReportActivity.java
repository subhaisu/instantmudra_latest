package com.mudra.instantmudra.cashout;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.mudra.instantmudra.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CashOutReportActivity extends Fragment {

    String BASE_URL="https://txnprc.instantmudra.co.in/appApi/TxnHistory";
    String userstr="";
    Context context;
    ArrayList<CashoutItemMOdel> cashout_report_arr;
    CashOut_RecyclerAdapter adapter;
    RecyclerView cash_out_recyclerV;
    ProgressBar progressV;

    public CashOutReportActivity(String userstr) {
        this.userstr = userstr;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.cashout_report_layout,container,false);
        cash_out_recyclerV=view.findViewById(R.id.cashout_recyclerV);
        progressV=view.findViewById(R.id.progressV);
        cashout_report_arr=new ArrayList<>();
        cash_out_recyclerV.setHasFixedSize ( true );
        cash_out_recyclerV.setLayoutManager ( new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false ) );
        fetchCashout_reportapi(BASE_URL,userstr);

        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void fetchCashout_reportapi(String url,String mobileno) {

        progressV.setVisibility(View.VISIBLE);
        JSONObject obj = new JSONObject();
        try {
            obj.put("mobile_no", mobileno);
            //obj.put("mobile_no", "7738611886");

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
//                                hideLoader();
                                cashout_report_arr.clear();
                                if (obj.getString("status").equalsIgnoreCase("success")) {
//                                    Toast.makeText(context, "Success !", Toast.LENGTH_SHORT).show();
                                    progressV.setVisibility(View.GONE);

                                    JSONArray jsonArray = obj.getJSONArray("param_c");

                                    for(int i =0 ; i<jsonArray.length();i++) {
                                        CashoutItemMOdel reportModel = new CashoutItemMOdel();
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String txnNo = jsonObject1.getString("txn_no");
                                        String rrn_No = jsonObject1.getString("rrn_no");
                                        String amount = jsonObject1.getString("amount");
                                        String txn_type = jsonObject1.getString("txn_type");
                                        String txn_status = jsonObject1.getString("txn_status");
                                        String balance_after_txn = jsonObject1.getString("balance_after_txn");
                                        String txn_date = jsonObject1.getString("txn_date");
                                        String aeps_no = jsonObject1.getString("aeps_no");

                                        reportModel.setAeps_no(aeps_no);
                                        reportModel.setAmount(amount);
                                        reportModel.setTxn_no(txnNo);
                                        reportModel.setRrn_no(rrn_No);
                                        reportModel.setTxn_type(txn_type);
                                        reportModel.setTxn_status(txn_status);
                                        reportModel.setBalance_after_txn(balance_after_txn);
                                        reportModel.setTxn_date(txn_date);
                                        cashout_report_arr.add(reportModel);

                                    }
                                    adapter=new CashOut_RecyclerAdapter(cashout_report_arr,context);
                                    cash_out_recyclerV.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    //Add adapter and design here

                                    if(cashout_report_arr.isEmpty()){
                                        Toast.makeText(getActivity(), "No data found.", Toast.LENGTH_SHORT).show();

                                    }

                                }
                                progressV.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                progressV.setVisibility(View.GONE);
//                                hideLoader();
                                Toast.makeText(getActivity(), "No data found.", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
//                            hideLoader();
                            progressV.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "No data found.", Toast.LENGTH_SHORT).show();

                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            progressV.setVisibility(View.GONE);
        }
    }
}
