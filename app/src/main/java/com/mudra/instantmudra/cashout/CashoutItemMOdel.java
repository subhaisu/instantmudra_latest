package com.mudra.instantmudra.cashout;

public class CashoutItemMOdel {

    String rrn_no;
    String amount;
    String txn_type;
    String txn_status;
    String balance_after_txn;
    String txn_date;
    String aeps_no;
    String txn_no;

    public String getRrn_no() {
        return rrn_no;
    }

    public void setRrn_no(String rrn_no) {
        this.rrn_no = rrn_no;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTxn_type() {
        return txn_type;
    }

    public void setTxn_type(String txn_type) {
        this.txn_type = txn_type;
    }

    public String getTxn_status() {
        return txn_status;
    }

    public void setTxn_status(String txn_status) {
        this.txn_status = txn_status;
    }

    public String getBalance_after_txn() {
        return balance_after_txn;
    }

    public void setBalance_after_txn(String balance_after_txn) {
        this.balance_after_txn = balance_after_txn;
    }

    public String getTxn_date() {
        return txn_date;
    }

    public void setTxn_date(String txn_date) {
        this.txn_date = txn_date;
    }

    public String getAeps_no() {
        return aeps_no;
    }

    public void setAeps_no(String aeps_no) {
        this.aeps_no = aeps_no;
    }

    public String getTxn_no() {
        return txn_no;
    }

    public void setTxn_no(String txn_no) {
        this.txn_no = txn_no;
    }
}
