package com.mudra.instantmudra.matm;

import com.mudra.instantmudra.model.BalanceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Url;

public interface BalanceApi {
        @GET()
        Call<BalanceResponse> getBalanceDetails(@Header("Authorization") String token, @Url() String url);
    }


