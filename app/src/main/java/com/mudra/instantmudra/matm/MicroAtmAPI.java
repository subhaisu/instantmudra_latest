package com.mudra.instantmudra.matm;

import com.mudra.instantmudra.model.MicroAtmRequestModel;
import com.mudra.instantmudra.model.MicroAtmResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface MicroAtmAPI {
    //sendEncryptedRequestNFS
    @POST()
    Call<MicroAtmResponse> checkRequestCode(@Header("Authorization") String token, @Body MicroAtmRequestModel body, @Url String url);
}
