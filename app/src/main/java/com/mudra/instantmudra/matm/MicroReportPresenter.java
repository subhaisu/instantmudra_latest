package com.mudra.instantmudra.matm;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;
import com.mudra.instantmudra.model.MicroReportModel;
import com.mudra.instantmudra.model.RefreshModel;
import com.mudra.instantmudra.model.RefreshRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class MicroReportPresenter implements MicroReportContract.UserActionsListener {

    /**
     * Initialize ReportContractView
     */
    private MicroReportContract.View microReportContractView;
    private AEPSAPIService aepsapiService;
    private ArrayList<MicroReportModel> microReportModelArrayList ;
    public String GET_MICRO_ATM_REPORT_URL = "https://itpl.iserveu.tech/generate/v8";
    /**
     * Initialize ReportPresenter
     */
    public MicroReportPresenter(MicroReportContract.View microReportContractView) {
        this.microReportContractView = microReportContractView;
    }

    @Override
    public void loadReports(final String fromDate, final String toDate, final String token, final String transactionType,final String matmtype) {
        if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
            microReportContractView.showLoader();
            if (this.aepsapiService == null) {
                this.aepsapiService = new AEPSAPIService();
            }
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            encryptedReport(fromDate,toDate,token,transactionType,encodedUrl,matmtype);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });


        } else {
            microReportContractView.emptyDates();
        }
    }



    public void encryptedReport(String fromDate, String toDate, String token,String type,String encodedUrl,String matmtype){
       /*final ReportAPI reportAPI =
           this.aepsapiService.getClient().create(ReportAPI.class);

       Call<ReportResponse> call = reportAPI.insertUser(token,new ReportRequest(fromDate,toDate,"AEPS"),encodedUrl);
*/
        //   "transactionType":"AEPS","fromDate":"2020-03-06","toDate":"2020-03-07"
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType",type);
//            jsonObject.put("transactionType","IATM_SETTLED");
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate",toDate);



            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                MicroReportResponse reportResponse =new MicroReportResponse() ;
                                ArrayList<MicroReportModel> reportModels = new ArrayList<>();
                                for(int i =0 ; i<jsonArray.length();i++){
                                    MicroReportModel reportModel = new MicroReportModel();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String op_type = jsonObject1.getString("operationPerformed");


                                    if (type.equalsIgnoreCase("MATM2")) {   //For MATM2


                                            reportModel.setId(jsonObject1.getLong("Id"));

                                            String previousAmount = jsonObject1.getString("previousAmount");
                                            if (!previousAmount.equalsIgnoreCase("null")) {
                                                reportModel.setPreviousAmount(Double.valueOf(previousAmount));
                                            } else {
                                                reportModel.setPreviousAmount("null");
                                            }

                                            String balanceAmount = jsonObject1.getString("balanceAmount");
                                            if (!balanceAmount.equalsIgnoreCase("null")) {
                                                reportModel.setBalanceAmount(Double.valueOf(balanceAmount));
                                            } else {
                                                reportModel.setBalanceAmount("null");
                                            }

                                            reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                            reportModel.setApiTId(jsonObject1.getString("apiTId"));
                                            reportModel.setStatus(jsonObject1.getString("status"));
                                            reportModel.setTransactionMode(jsonObject1.getString("operationPerformed"));
                                            reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                    /*reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setBankName(jsonObject1.getString("bankName"));
*/

                                            reportModel.setUserName(jsonObject1.getString("userName"));
                                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                                            reportModel.setUserTrackId(jsonObject1.getString("userTrackId"));
                                            reportModel.setCardDetail(jsonObject1.getString("cardDetail"));
                                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                            // reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                            reportModels.add(reportModel);

                                    }

                                    else {

                                        if(matmtype.equalsIgnoreCase(op_type)) {


                                            reportModel.setId(jsonObject1.getInt("Id"));

                                            String previousAmount = jsonObject1.getString("previousAmount");
                                            if (!previousAmount.equalsIgnoreCase("null")) {
                                                reportModel.setPreviousAmount(Double.valueOf(previousAmount));
                                            } else {
                                                reportModel.setPreviousAmount("null");
                                            }

                                            String balanceAmount = jsonObject1.getString("balanceAmount");
                                            if (!balanceAmount.equalsIgnoreCase("null")) {
                                                reportModel.setBalanceAmount(Double.valueOf(balanceAmount));
                                            } else {
                                                reportModel.setBalanceAmount("null");
                                            }

                                            reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                            reportModel.setApiTId(jsonObject1.getString("apiTId"));
                                            reportModel.setStatus(jsonObject1.getString("status"));
                                            reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                            reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                    /*reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setBankName(jsonObject1.getString("bankName"));
*/

                                            reportModel.setUserName(jsonObject1.getString("userName"));
                                            reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                            reportModel.setMasterName(jsonObject1.getString("masterName"));
                                            reportModel.setUserTrackId(jsonObject1.getString("userTrackId"));
                                            reportModel.setCardDetail(jsonObject1.getString("cardDetail"));
                                            reportModel.setCreatedDate(String.valueOf(jsonObject1.getLong("createdDate")));
                                            reportModel.setUpdatedDate(String.valueOf(jsonObject1.getLong("updatedDate")));
                                            // reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                            reportModels.add(reportModel);
                                        }
                                    }

                                }
                                Collections.reverse(reportModels);
                                reportResponse.setmATMTransactionReport(reportModels);


                                // Gson gson = new Gson();
                                // reportResponse = gson.fromJson(jsonArray.toString(),ReportResponse.class);

                                /*if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                                    ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                                    double totalAmount = 0;
                                    for (int i = 0; i < result.size(); i++) {
                                        totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                                    }
                                    reportView.reportsReady(result, String.valueOf(totalAmount));
                                }*/


                                if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                                    ArrayList<MicroReportModel> result = new ArrayList<>();
                                    result.clear();
                                    result=reportResponse.getmATMTransactionReport();
                                    double totalAmount = 0;
                                    for(int i = 0; i<result.size(); i++) {
                                        totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                                    }
                                    microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                                }
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                                //  System.out.println(obj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                microReportContractView.hideLoader();
                                microReportContractView.showReports();
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            microReportContractView.hideLoader();
                            microReportContractView.showReports();
                        }
                    });








        }catch (Exception e){
            e.printStackTrace();
        }









       /*call.enqueue(new Callback<ReportResponse>() {
           @Override
           public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                *//*response.body(); // have your all data
                String userName = response.body().getStatus();*//*
               if(response.isSuccessful()) {

                   ReportResponse reportResponse = response.body();
                   Log.v("Laxmi","hfh"+reportResponse);

                   if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                       ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                       double totalAmount = 0;
                       for(int i = 0; i<result.size(); i++) {
                           totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                       }
                       reportView.reportsReady(result, String.valueOf(totalAmount));
                   }
               }
               reportView.hideLoader();
               reportView.showReports();
           }

           @Override
           public void onFailure(Call<ReportResponse> call, Throwable t) {

               reportView.hideLoader();
               reportView.showReports();

           }
       });*/
    }







   /* public void encyptedMicroReport(String fromDate, String toDate, String token, String transactionType, String encodedUrl){
        MicroReportApi reportAPI =
            this.aepsapiService.getClient().create(MicroReportApi.class);

        Call<MicroReportResponse> call = reportAPI.insertUser(token,new MicroReportRequest(fromDate,toDate,transactionType),encodedUrl);

        call.enqueue(new Callback<MicroReportResponse>() {
            @Override
            public void onResponse(Call<MicroReportResponse> call, Response<MicroReportResponse> response) {
                if(response.isSuccessful()) {

                    MicroReportResponse reportResponse = response.body();
                    if (reportResponse != null && reportResponse.getmATMTransactionReport() != null) {
                        ArrayList<MicroReportModel> result = reportResponse.getmATMTransactionReport();
                        double totalAmount = 0;
                        for(int i = 0; i<result.size(); i++) {
                            totalAmount += Double.parseDouble(result.get(i).getAmountTransacted());
                        }
                        microReportContractView.reportsReady(result, String.valueOf(totalAmount));
                    }
                }
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }

            @Override
            public void onFailure(Call<MicroReportResponse> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.showReports();
            }
        });
    }
*/
   @Override
   public void refreshReports(final String token, final String amount,final String transactionType, final String transactionMode,  final String clientUniqueId) {
       microReportContractView.showLoader();
       if(amount == null || amount.matches("")){
           microReportContractView.checkAmount("1");
           return;
       }
       if(transactionMode ==null || transactionMode.matches("")){
           microReportContractView.checkTransactionMode("1");
           return;
       }
       if(transactionType == null || transactionType.matches("")){
           microReportContractView.checkTransactionType("1");
           return;
       }
       if(clientUniqueId == null || clientUniqueId.matches("")){
           microReportContractView.checkClientId("1");
           return;
       }

       AndroidNetworking.get(GET_MICRO_ATM_REPORT_URL)
               .setPriority(Priority.HIGH)
               .build()
               .getAsJSONObject(new JSONObjectRequestListener() {
                   @Override
                   public void onResponse(JSONObject response) {
                       try {
                           JSONObject obj = new JSONObject(response.toString());
                           String key = obj.getString("hello");
                           System.out.println(">>>>-----"+key);
                           byte[] data = Base64.decode(key,Base64.DEFAULT);
                           String encodedUrl = new String(data, "UTF-8");

                           encryptLoadReport(token,amount,transactionType,transactionMode,clientUniqueId,encodedUrl);

                       } catch (JSONException e) {
                           e.printStackTrace();
                       } catch (UnsupportedEncodingException e) {
                           e.printStackTrace();
                       }


                   }

                   @Override
                   public void onError(ANError anError) {

                   }
               });

   }


    /**
     *  load Reports of  ReportActivity
     */

    public void encryptLoadReport(String token, final String amount, final String transactionType, final String transactionMode, final String clientUniqueId,String encodedUrl){
        RefreshApi reportAPI =
                this.aepsapiService.getClient().create(RefreshApi.class);

        Call<RefreshModel> call = reportAPI.insertUser(token,new RefreshRequest(amount,transactionType,transactionMode,clientUniqueId),encodedUrl);

        call.enqueue(new Callback<RefreshModel>() {
            @Override
            public void onResponse(Call<RefreshModel> call, Response<RefreshModel> response) {

                if(response.isSuccessful()){
                    RefreshModel refreshModel = response.body();
                    if (refreshModel != null) {
                        microReportContractView.hideLoader();
                        microReportContractView.refreshDone(refreshModel);
                    }
                }else{
                    microReportContractView.hideLoader();
                    microReportContractView.emptyRefreshData("1");
                }
            }

            @Override
            public void onFailure(Call<RefreshModel> call, Throwable t) {
                microReportContractView.hideLoader();
                microReportContractView.emptyRefreshData("1");
            }
        });
    }


}
