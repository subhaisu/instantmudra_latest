package com.mudra.instantmudra.matm;


import com.mudra.instantmudra.model.MicroAtmRequestModel;
import com.mudra.instantmudra.model.MicroAtmResponse;
import com.mudra.instantmudra.model.UserInfoModel;

import java.util.ArrayList;

public class MicroAtmContract {


    /**
     * View interface sends report list to MainActivity
     */
    public interface View {

        /**
         * checkLoginStatus() checks  whether login is a failure or success. Status "0" is failure and Status "1" is success
         */
        void checkRequestCode(String status, String message, MicroAtmResponse microAtmResponse);
        /**
         * checkEmptyFields() validates whether username and password are empty
         */
        void showFeature(ArrayList<UserInfoModel.userFeature> userFeatures);

        void checkEmptyFields();
        void showLoader();
        void hideLoader();


    }

    /**
     * UserActionsListener interface checks the load of Reports
     */
    interface UserActionsListener {
        void performRequestData(String token, MicroAtmRequestModel microAtmRequestModel);
        void loadFeature(String token);

    }

}
