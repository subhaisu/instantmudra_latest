package com.mudra.instantmudra.reports;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.mudra.instantmudra.ConstantClass;
import com.mudra.instantmudra.R;
import com.mudra.instantmudra.SessionManager;
import com.mudra.instantmudra.Util;
import com.mudra.instantmudra.adapters.AEPS2ReportRecyclerViewAdapter;
import com.mudra.instantmudra.adapters.ReportRecyclerViewAdapter;
import com.mudra.instantmudra.aeps.ReportContract;
import com.mudra.instantmudra.aeps.ReportPresenter;
import com.mudra.instantmudra.model.AEPS2ReportModel;
import com.mudra.instantmudra.model.ReportModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


public class AepsFragment extends Fragment implements ReportContract.View, DatePickerDialog.OnDateSetListener {


    private RecyclerView reportRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ReportPresenter mActionsListener;
    ArrayList<ReportModel> reportResponseArrayList = new ArrayList<ReportModel>();
    ArrayList<AEPS2ReportModel> aeps2reportResponseArrayList = new ArrayList<AEPS2ReportModel>();
    LinearLayout dateLayout,detailsLayout;
    private ReportRecyclerViewAdapter reportRecyclerviewAdapter;
    private AEPS2ReportRecyclerViewAdapter aeps2reportRecyclerviewAdapter;

    TextView noData,totalreport,amount;
    TextView chooseDateRange;
    private static final String TAG = AepsFragment.class.getSimpleName ();
    SessionManager session;
    String tokenStr="";
    ProgressBar progressV;
    Context context;

    Button aeps1,aeps2;
    String fromdate="",todate="";
    Boolean calenderFlag = false;
    String providerType = "AEPS";

    public AepsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_aeps, container, false);
        initView(rootview);

        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
//        else if(id == R.id.filterBar){
//            callFilterFunction(item);
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                try {
                if(providerType.equalsIgnoreCase("AEPS")){
                    if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                        reportRecyclerviewAdapter.getFilter().filter(s);
                        reportRecyclerView.getRecycledViewPool().clear();
                        reportRecyclerviewAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                    }
                }else{
                    if (aeps2reportResponseArrayList!=null && aeps2reportResponseArrayList.size() > 0) {
                        // filter recycler view when text is changed
                        aeps2reportRecyclerviewAdapter.getFilter().filter(s);
                        reportRecyclerView.getRecycledViewPool().clear();
                        aeps2reportRecyclerviewAdapter.notifyDataSetChanged();
                    }else{
                        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                    }
                }
                }catch (Exception e){

                }


                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);





    }



    private void initView(View view) {
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setTitle("AePS Report");

        aeps1 = view.findViewById(R.id.aeps1);
        aeps2 = view.findViewById(R.id.aeps2);

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressV = view.findViewById(R.id.progressV);
        noData = view.findViewById ( R.id.noData );
        amount = view.findViewById ( R.id.amount );
        totalreport = view.findViewById ( R.id.totalreport );
        detailsLayout = view.findViewById ( R.id.detailsLayout );
        chooseDateRange = view.findViewById ( R.id.chooseDateRange );
        dateLayout = view.findViewById ( R.id.dateLayout );
        reportRecyclerView = view.findViewById ( R.id.reportRecyclerView );

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportRecyclerView.setHasFixedSize ( true );
        reportRecyclerView.setLayoutManager(mLayoutManager);

        mActionsListener = new ReportPresenter (AepsFragment.this);

        aeps1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if(calenderFlag){
                try {
                    aeps1.setTextColor(getResources().getColor(R.color.light_grey));
                    aeps2.setTextColor(getResources().getColor(R.color.white));
                    if(!reportResponseArrayList.isEmpty()){
                        reportResponseArrayList.clear();
                        if(reportRecyclerviewAdapter!=null) {
                            reportRecyclerviewAdapter.notifyDataSetChanged();
                        }
                    }
                    providerType = "AEPS";
                    CallReportApi(providerType);
                }catch (Exception e){

                }
            /*}else{
                Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
            }*/
            }
        });

        aeps2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if(calenderFlag){
                try {
                    aeps2.setTextColor(getResources().getColor(R.color.light_grey));
                    aeps1.setTextColor(getResources().getColor(R.color.white));

                    if(!aeps2reportResponseArrayList.isEmpty()){
                        aeps2reportResponseArrayList.clear();
                        if(reportRecyclerviewAdapter!=null) {
                            reportRecyclerviewAdapter.notifyDataSetChanged();
                        }
                    }
                    providerType = "AEPS2";
                    CallReportApi(providerType);
                }catch (Exception e){

                }
               /* }else{
                    Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }






    @Override
    public void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount) {
        try {
            reportResponseArrayList = reportModelArrayList;
            amount.setText(getResources().getString(R.string.toatlamountacitvity)+totalAmount);
        }catch (Exception e){
        }
       }

    @Override
    public void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount) {
       try {
        aeps2reportResponseArrayList = reportModelArrayList;
        amount.setText(getResources().getString(R.string.toatlamountacitvity)+totalAmount);
       }catch (Exception e){
       }
    }

    @Override
    public void showReports() {

        try {
        if(providerType.equalsIgnoreCase("AEPS")){

            if (reportResponseArrayList!=null && reportResponseArrayList.size() > 0) {
                reportRecyclerView.setVisibility(View.VISIBLE);
                detailsLayout.setVisibility(View.VISIBLE);
                noData.setVisibility(View.GONE);
                reportRecyclerviewAdapter = new ReportRecyclerViewAdapter(reportResponseArrayList,context);
                reportRecyclerView.setAdapter(reportRecyclerviewAdapter);
                totalreport.setText("Entries: "+reportResponseArrayList.size());
            }else{
                reportRecyclerView.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                totalreport.setText("Entries: "+ 0);
            }

        }else{
            if (aeps2reportResponseArrayList!=null && aeps2reportResponseArrayList.size() > 0) {
                reportRecyclerView.setVisibility(View.VISIBLE);
                detailsLayout.setVisibility(View.VISIBLE);
                noData.setVisibility(View.GONE);
                try {

                    aeps2reportRecyclerviewAdapter = new AEPS2ReportRecyclerViewAdapter(aeps2reportResponseArrayList,context);
                    reportRecyclerView.setAdapter(aeps2reportRecyclerviewAdapter);
                    totalreport.setText("Entries: "+aeps2reportResponseArrayList.size());

                }catch (Exception e){

                }

            }else{
                reportRecyclerView.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
                totalreport.setText("Entries: "+ 0);
            }
        }

        }catch (Exception e){

        }
    }

    @Override
    public void showLoader() {
        progressV.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoader() {
        progressV.setVisibility(View.GONE);
    }

    @Override
    public void emptyDates() {
        Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();

    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(AepsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate ( Calendar.getInstance () );
        dpd.setAutoHighlight ( true );

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + dayOfMonthEnd;
        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+dayOfMonthEnd+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        // chooseDateRange.setText ( date );
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setSubtitle(date);


        if(date !=null && !date.matches ( "" ) && !date.trim ().matches ( "Choose Date Range for Reports" )) {
            calenderFlag = true;
            if(providerType.isEmpty()){
                Util.showAlert(getActivity(),"Alert","Please Select Operation Performed.");
            }else{
                CallReportApi(providerType);
            }
        }else{
        }
    }
    //==============
    public  void CallReportApi(String providerType){
        if(calenderFlag){
            if (!(Util.getDateDiff ( new SimpleDateFormat( "yyyy-MM-dd" ),fromdate, todate ) > 10)) {
                if (Util.compareDates ( fromdate, todate ) == "1") {
                    // noData.setVisibility ( View.GONE );
                    //reportRecyclerView.setVisibility ( View.VISIBLE );
                    String ToDt = Util.getNextDate (todate);
                    //setListener(fromdate,ToDt);
                    tokenStr = ConstantClass.TOKEN;
                    mActionsListener.loadReports ( fromdate, Util.getNextDate (todate), tokenStr,providerType);
                } else if (Util.compareDates ( fromdate, todate ) == "2") {

                } else if(Util.compareDates ( fromdate,todate) == "3" ) {
                    tokenStr = ConstantClass.TOKEN;
                    mActionsListener.loadReports ( fromdate,Util.getNextDate (todate), tokenStr,providerType);

                }
            }else{
                Toast.makeText(getActivity(), "Report shown between 10 days.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(getActivity(), "Select start date and end date.", Toast.LENGTH_SHORT).show();
        }
    }

}
