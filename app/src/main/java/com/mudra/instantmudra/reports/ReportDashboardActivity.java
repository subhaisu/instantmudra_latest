package com.mudra.instantmudra.reports;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.tabs.TabLayout;
import com.mudra.instantmudra.ConstantClass;
import com.mudra.instantmudra.R;
import com.mudra.instantmudra.SessionManager;
import com.mudra.instantmudra.cashout.CashOutReportActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class ReportDashboardActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private LinearLayout fragment_container;
    private TabLayout tablayout;


    Handler timer_handler;
    String _userName,mobileNo="";
    boolean session_logout = false;
    SessionManager session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dashboard);
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);
        mobileNo=getIntent().getStringExtra("mobileNo");

       // getUserAuthToken();

        setUpToolBar();
        initView();
        callTablistener();
    }


    private void getUserAuthToken(){
        String url = "https://mobile.9fin.co.in/api/getAuthenticateData" ;
        JSONObject obj = new JSONObject();
        try {
            obj.put("encryptedData", ConstantClass.ENCRIPTED_DATA);
            obj.put("retailerUserName",ConstantClass.LOGIN_ID);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");

                                if(status.equalsIgnoreCase("success")) {
                                    String userName = obj.getString("username");
                                    String userToken = obj.getString("usertoken");

                                    ConstantClass.TOKEN = userToken;


                                }else {
                                    showAlert(status);

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                                showAlert("Invalid Encrypted Data");
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            //hideLoader();

                            showAlert("Internal Server Error.");

                        }

                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }


    private void setUpToolBar() {
        // Inflate the layout for this fragment
        mToolbar = findViewById ( R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle("");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });
    }
    private void initView() {
        tablayout = findViewById(R.id.tablayout);
        fragment_container = findViewById(R.id.fragment_container);


    }

    private void callTablistener() {
       // replaceFragment(new AepsFragment());
        replaceFragment(new CashOutReportActivity(mobileNo));

        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new AepsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("AePS Report");
                }
                else if (tab.getPosition() == 1) {
                    replaceFragment(new MatmFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("mATM Report");
                }
//                else if(tab.getPosition() == 2) {
////                    replaceFragment(new DmtFragment());
//                    getSupportActionBar().setSubtitle("");
//                    getSupportActionBar().setTitle("DMT Report");
//                }
                else {
                    replaceFragment(new CashOutReportActivity(mobileNo));
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Cashout");

//                    Intent intent=new Intent(ReportDashboardActivity.this,CashOutReportActivity.class);
//                    intent.putExtra("mobile_no",mobileNo);
//                    startActivity(intent);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.commit();
    }


/*    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null && session_logout == false) {
            checkSessionExistance(_userName,true);
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(ReportDashboardActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(ReportDashboardActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(ReportDashboardActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(ReportDashboardActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/

    public void showAlert(String msg){

        AlertDialog.Builder builder = new AlertDialog.Builder(ReportDashboardActivity.this);
        builder.setTitle("Alert!!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.show();
    }
}
