package com.mudra.instantmudra.aeps;

import com.mudra.instantmudra.model.AEPS2ReportModel;

import java.util.ArrayList;

public class AEPS2ReportResponse {
    ArrayList<AEPS2ReportModel> AepsreportList =new ArrayList<>();

    public ArrayList<AEPS2ReportModel> getAepsreportList() {
        return AepsreportList;
    }

    public void setAepsreportList(ArrayList<AEPS2ReportModel> aepsreportList) {
        AepsreportList = aepsreportList;
    }
}
