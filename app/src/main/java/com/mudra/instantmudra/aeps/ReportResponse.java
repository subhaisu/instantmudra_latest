package com.mudra.instantmudra.aeps;

import com.mudra.instantmudra.model.ReportModel;

import java.util.ArrayList;

public class ReportResponse {
    ArrayList<ReportModel> AepsreportList =new ArrayList<>();

    public ArrayList<ReportModel> getAepsreportList() {
        return AepsreportList;
    }

    public void setAepsreportList(ArrayList<ReportModel> aepsreportList) {
        AepsreportList = aepsreportList;
    }
}
