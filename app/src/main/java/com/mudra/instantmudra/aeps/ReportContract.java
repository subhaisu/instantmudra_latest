package com.mudra.instantmudra.aeps;

import com.mudra.instantmudra.model.AEPS2ReportModel;
import com.mudra.instantmudra.model.ReportModel;

import java.util.ArrayList;


public class ReportContract {

    public interface View {

        void reportsReady(ArrayList<ReportModel> reportModelArrayList, String totalAmount);
        void AEPS2reportsReady(ArrayList<AEPS2ReportModel> reportModelArrayList, String totalAmount);
        void showReports();
        void showLoader();
        void hideLoader();
        void emptyDates();


    }

    interface UserActionsListener {
        void loadReports(String fromDate, String toDate, String token, String providerType);
    }


}

