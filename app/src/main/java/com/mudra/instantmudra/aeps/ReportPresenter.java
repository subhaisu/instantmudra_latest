package com.mudra.instantmudra.aeps;

import android.util.Base64;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;
import com.mudra.instantmudra.model.AEPS2ReportModel;
import com.mudra.instantmudra.model.ReportModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


public class ReportPresenter implements ReportContract.UserActionsListener {
        /**
         * Initialize ReportContractView
         */
        private ReportContract.View reportView;
        private AEPSAPIService aepsapiService;
        private ArrayList<ReportResponse> reportResponseArrayList ;
        /**
         * Initialize ReportPresenter
         */
        public ReportPresenter(ReportContract.View reportView) {
                this.reportView = reportView;
        }

        @Override
        public void loadReports(final String fromDate,final String toDate,final String token, String providerType) {

                if (fromDate != null && !fromDate.matches("") && toDate != null && !toDate.matches("") ) {
                        reportView.showLoader();
                        if (this.aepsapiService == null) {
                                this.aepsapiService = new AEPSAPIService();
                        }

                        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                                .setPriority(Priority.HIGH)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                                try {
                                                        JSONObject obj = new JSONObject(response.toString());
                                                        String key = obj.getString("hello");
                                                        System.out.println(">>>>-----"+key);
                                                        byte[] data = Base64.decode(key,Base64.DEFAULT);
                                                        String encodedUrl = new String(data, "UTF-8");
                                                        // {"transactionType":"AEPS","fromDate":"2020-03-06","toDate":"2020-03-07"}
                                                        encryptedReport(fromDate,toDate,token,encodedUrl,providerType);

                                                } catch (JSONException e) {
                                                        e.printStackTrace();
                                                } catch (UnsupportedEncodingException e) {
                                                        e.printStackTrace();
                                                }


                                        }

                                        @Override
                                        public void onError(ANError anError) {

                                        }
                                });


                } else {
                        reportView.emptyDates();

                        //loginView.checkEmptyFields();
                }
        }

        public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl,String providerType){
       /*final ReportAPI reportAPI =
           this.aepsapiService.getClient().create(ReportAPI.class);

       Call<ReportResponse> call = reportAPI.insertUser(token,new ReportRequest(fromDate,toDate,"AEPS"),encodedUrl);
*/
                //   "transactionType":"AEPS","fromDate":"2020-03-06","toDate":"2020-03-07"

//            encodedUrl = "https://uatapps.iserveu.online/STAGEING/BQ/transactiondetails?";

                try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("transactionType",providerType);
                        jsonObject.put("fromDate",fromDate);
                        jsonObject.put("toDate",toDate);

                        AndroidNetworking.post(encodedUrl)
                                .setPriority(Priority.HIGH)
                                .addHeaders("Authorization",token)
                                .addJSONObjectBody(jsonObject)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                                try {
                                                        JSONObject obj = new JSONObject(response.toString());
                                                        // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                                        JSONArray jsonArray = obj.getJSONArray("BQReport");

                                                        if(providerType.equalsIgnoreCase("AEPS")) {

                                                            ReportResponse reportResponse = new ReportResponse();
                                                            ArrayList<ReportModel> reportModels = new ArrayList<>();
                                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                                ReportModel reportModel = new ReportModel();
                                                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                                                reportModel.setId(jsonObject1.getLong("Id"));
                                                                reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                                                                reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                                                                reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                                                reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                                                reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                                                reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                                                reportModel.setBankName(jsonObject1.getString("bankName"));
                                                                reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                                                reportModel.setStatus(jsonObject1.getString("status"));
                                                                reportModel.setUserName(jsonObject1.getString("userName"));
                                                                reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                                                reportModel.setMasterName(jsonObject1.getString("masterName"));
                                                                reportModel.setAPI(jsonObject1.getString("API"));
                                                                reportModel.setUserTrackId(jsonObject1.getString("userTrackId"));
                                                                reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                                                reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                                                reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                                                reportModels.add(reportModel);
                                                            }
                                                            //Collections.reverse(reportModels);
                                                            reportResponse.setAepsreportList(reportModels);

                                                            if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                                                                ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                                                                double totalAmount = 0;
                                                                for (int i = 0; i < result.size(); i++) {
                                                                    totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                                                                }
                                                                reportView.reportsReady(result, String.valueOf(totalAmount));
                                                            }
                                                            reportView.hideLoader();
                                                            reportView.showReports();
                                                        }else{
                                                            AEPS2ReportResponse reportResponse = new AEPS2ReportResponse();
                                                            ArrayList<AEPS2ReportModel> reportModels = new ArrayList<>();
                                                            for (int i = 0; i < jsonArray.length(); i++) {
                                                                AEPS2ReportModel reportModel = new AEPS2ReportModel();
                                                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                                                reportModel.setId(jsonObject1.getLong("Id"));
                                                                reportModel.setPreviousAmount(jsonObject1.getString("previousAmount"));
                                                                reportModel.setBalanceAmount(jsonObject1.getString("balanceAmount"));
                                                                reportModel.setAmountTransacted(jsonObject1.getString("amountTransacted"));
                                                                reportModel.setOperationPerformed(jsonObject1.getString("operationPerformed"));
                                                                reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                                                reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                                               // reportModel.setBankName(jsonObject1.getString("bankName"));
                                                                reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                                                reportModel.setStatus(jsonObject1.getString("status"));
                                                                reportModel.setUserName(jsonObject1.getString("userName"));
                                                                reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                                                reportModel.setMasterName(jsonObject1.getString("masterName"));
                                                               // reportModel.setAPI(jsonObject1.getString("API"));
                                                                //reportModel.setUserTrackId(jsonObject1.getString("userTrackId"));
                                                                /*if(jsonObject1.getString("createdDate")!=null){
                                                                    reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                                                }
                                                                if(jsonObject1.getString("updatedDate")!=null){
                                                                    reportModel.setCreatedDate(jsonObject1.getLong("updatedDate"));
                                                                }*/

                                                               reportModel.setCreatedDate(jsonObject1.getString("createdDate"));
                                                                reportModel.setUpdatedDate(jsonObject1.getString("updatedDate"));
                                                                reportModel.setReferenceNo(jsonObject1.getString("referenceNo"));
                                                                reportModels.add(reportModel);
                                                            }
                                                           // Collections.reverse(reportModels);
                                                            reportResponse.setAepsreportList(reportModels);

                                                            if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                                                                ArrayList<AEPS2ReportModel> result = reportResponse.getAepsreportList();
                                                                double totalAmount = 0;
                                                                for (int i = 0; i < result.size(); i++) {
                                                                    if(!result.get(i).getAmountTransacted().equalsIgnoreCase("null")){
                                                                    totalAmount += Double.parseDouble(result.get(i).getAmountTransacted());}
                                                                }
                                                                reportView.AEPS2reportsReady(result, String.valueOf(totalAmount));
                                                            }
                                                            reportView.hideLoader();
                                                            reportView.showReports();


                                                        }
                                                } catch (JSONException e) {
                                                        e.printStackTrace();
                                                        reportView.hideLoader();
                                                        reportView.showReports();
                                                }
                                        }

                                        @Override
                                        public void onError(ANError anError) {
                                                anError.getErrorDetail();
                                                reportView.hideLoader();
                                                reportView.showReports();
                                        }
                                });




                }catch (Exception e){
                        e.printStackTrace();
                }









       /*call.enqueue(new Callback<ReportResponse>() {
           @Override
           public void onResponse(Call<ReportResponse> call, Response<ReportResponse> response) {
                *//*response.body(); // have your all data
                String userName = response.body().getStatus();*//*
               if(response.isSuccessful()) {

                   ReportResponse reportResponse = response.body();
                   Log.v("Laxmi","hfh"+reportResponse);

                   if (reportResponse != null && reportResponse.getAepsreportList() != null) {
                       ArrayList<ReportModel> result = reportResponse.getAepsreportList();
                       double totalAmount = 0;
                       for(int i = 0; i<result.size(); i++) {
                           totalAmount += Double.parseDouble(String.valueOf(result.get(i).getAmountTransacted()));
                       }
                       reportView.reportsReady(result, String.valueOf(totalAmount));
                   }
               }
               reportView.hideLoader();
               reportView.showReports();
           }

           @Override
           public void onFailure(Call<ReportResponse> call, Throwable t) {

               reportView.hideLoader();
               reportView.showReports();

           }
       });*/
        }

}
