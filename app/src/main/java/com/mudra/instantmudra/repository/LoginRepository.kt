package com.mudra.instantmudra.repository

import android.app.Application
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit
import java.util.regex.Pattern

class LoginRepository (application: Application) {

    var application:Application
    val SuccessMsg = MutableLiveData<String>()



    init {
        this.application = application
    }

    fun validateCredentials(phoneNo:String,password:String): LiveData<String> {
        val loginErrorMessage = MutableLiveData<String>()

        if(phoneNo.length<10){
            loginErrorMessage.value = "Phone no minimum 10 digit.";
        }else if(password.length<6){
            loginErrorMessage.value = "Password should be minimum 6 digit.";
        }
        else{

           // ValidateDataInFireBase(phoneNo);
            sendVerificationCode(phoneNo);
        }
        return  loginErrorMessage
    }



    private fun sendVerificationCode(phoneNo: String) {
       // PhoneAuthProvider.getInstance().verifyPhoneNumber("+91"+phoneNo,60,TimeUnit.SECONDS,TaskExecutors.MAIN_THREAD,mCallbacks)


    }



    private fun ValidateDataInFireBase(phnpStr:String) {
        var mcallBack = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks(){


            override fun onVerificationCompleted(crediential: PhoneAuthCredential) {
                if (crediential != null) {
                  //  signInWithPhoneAuthCredential(crediential)
                }
            }

            override fun onVerificationFailed(p0: FirebaseException) {
                TODO("Not yet implemented")

                Log.d("Rajesh","Invalid phone number or verification failed.")

            }

        }


       /* var mCallBacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential?) {
                if (credential != null) {
                    signInWithPhoneAuthCredential(credential)
                }
            }

            override fun onVerificationFailed(p0: FirebaseException?) {
                progressBar.visibility = View.GONE
                toast("Invalid phone number or verification failed.")
            }
            override fun onCodeSent(verificationId: String?, token: PhoneAuthProvider.ForceResendingToken?) {
                super.onCodeSent(verificationId, token)
                progressBar.visibility = View.GONE
                verificationID = verificationId.toString()
                token_ = token.toString()

                etNumber.setText("")

                etNumber.setHint("Enter OTP ")
                btnSignIn.setText("Verify OTP")

                btnSignIn.setOnClickListener {
                    progressBar.visibility = View.VISIBLE
                    verifyAuthentication(verificationID, etNumber.text.toString())
                }

                Log.e("Login : verificationId ", verificationId)
                Log.e("Login : token ", token_)

            }
    }}

    private fun verifyVerificationCode(phnpStr: String) {


    }
*/





    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun isPasswordValid(password: String): Boolean{
        val expression  ="^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\\\S+\$).{4,}\$";
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }
}


}
