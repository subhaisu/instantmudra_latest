package com.mudra.instantmudra.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.mudra.instantmudra.repository.LoginRepository

class LoginViewModel:AndroidViewModel {

    private var validationRepository:LoginRepository

    constructor(application: Application) : super(application){
        validationRepository = LoginRepository(application)
    }

    fun validateCredentials(phoneNo:String,passWord:String): LiveData<String> {
        return validationRepository.validateCredentials(phoneNo,passWord)
    }


}