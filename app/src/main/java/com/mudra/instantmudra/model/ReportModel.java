package com.mudra.instantmudra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReportModel {
    @SerializedName("Id")
    @Expose
    private long id;

    @SerializedName("previousAmount")
    @Expose
    private Object previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private Integer amountTransacted;
    @SerializedName("balanceAmount")
    @Expose
    private Object balanceAmount;
    @SerializedName("operationPerformed")
    @Expose
    private String operationPerformed;
    @SerializedName("apiTid")
    @Expose
    private String apiTid;
    @SerializedName("apiComment")
    @Expose
    private String apiComment;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;
    @SerializedName("API")
    @Expose
    private String aPI;
    @SerializedName("userTrackId")
    @Expose
    private String userTrackId;
    @SerializedName("createdDate")
    @Expose
    private long createdDate;
    @SerializedName("updatedDate")
    @Expose
    private long updatedDate;
    @SerializedName("referenceNo")
    @Expose
    private String referenceNo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Object getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(Object previousAmount) {
        this.previousAmount = previousAmount;
    }

    public Integer getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(Integer amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public Object getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Object balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getApiTid() {
        return apiTid;
    }

    public void setApiTid(String apiTid) {
        this.apiTid = apiTid;
    }

    public String getApiComment() {
        return apiComment;
    }

    public void setApiComment(String apiComment) {
        this.apiComment = apiComment;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getAPI() {
        return aPI;
    }

    public void setAPI(String aPI) {
        this.aPI = aPI;
    }

    public String getUserTrackId() {
        return userTrackId;
    }

    public void setUserTrackId(String userTrackId) {
        this.userTrackId = userTrackId;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long createdDate) {
        this.createdDate = createdDate;
    }

    public long getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(long updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

}
