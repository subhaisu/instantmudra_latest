package com.mudra.instantmudra.model

import com.google.gson.annotations.SerializedName

public class Cashout_response_model {

    @SerializedName("status")
    val status: String? = ""

    @SerializedName("statusDesc")
    val statusDesc: String? = ""

    @SerializedName("data")
    val cashoutDataobj: CashOut_DataObj ?=null

    constructor(
        Txn_Ref_no:String,
        status:String,
        statusDesc:String,
        cashoutDataobj:CashOut_DataObj
    ){

    }


    override fun toString(): String {
        return "Cashout_response_model(status=$status, " +
                "statusDesc=$statusDesc," +
                " cashoutDataobj=$cashoutDataobj)"
    }


}