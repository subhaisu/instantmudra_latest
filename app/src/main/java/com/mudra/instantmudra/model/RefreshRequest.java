package com.mudra.instantmudra.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RefreshRequest {

    @SerializedName("amount")
    @Expose
    private String amount;

    @SerializedName("transactionType")
    @Expose
    private String transactionType;

    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;

    @SerializedName("clientUniqueId")
    @Expose
    private String clientUniqueId;

    public RefreshRequest(String amount, String transactionType, String transactionMode, String clientUniqueId) {
        this.amount = amount;
        this.transactionType = transactionType;
        this.transactionMode = transactionMode;
        this.clientUniqueId = clientUniqueId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getClientUniqueId() {
        return clientUniqueId;
    }

    public void setClientUniqueId(String clientUniqueId) {
        this.clientUniqueId = clientUniqueId;
    }
}
