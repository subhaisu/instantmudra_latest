package com.mudra.instantmudra.model

import com.google.gson.annotations.SerializedName

public class CashOut_DataObj {

    @SerializedName("prepaid_wallet_balance")
    val prepaid_wallet_balance: String? = ""

    @SerializedName("postpaid_wallet_balance")
    val postpaid_wallet_balance: String? = ""
    @SerializedName("txn_no")
    val txn_no: String? = ""
    @SerializedName("amount")
    val amount: String? = ""

    constructor(
        prepaid_wallet_balance: String,
        postpaid_wallet_balance: String,
        txn_no: String,
        amount: String
    ){

    }

    override fun toString(): String {
        return "CashOut_DataObj(prepaid_wallet_balance=$prepaid_wallet_balance," +
                "postpaid_wallet_balance=$postpaid_wallet_balance" +
                "txn_no=$txn_no" +
                "amount=$amount)"
    }
}