package com.mudra.instantmudra

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mudra.instantmudra.view.LoginFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().replace(R.id.container_basic_login, LoginFragment()
        ).commit()
    }
}
